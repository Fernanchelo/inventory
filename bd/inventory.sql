-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 18-09-2020 a las 22:46:44
-- Versión del servidor: 10.4.10-MariaDB
-- Versión de PHP: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `inventory`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin_materiaprima`
--

DROP TABLE IF EXISTS `admin_materiaprima`;
CREATE TABLE IF NOT EXISTS `admin_materiaprima` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `proveedor` int(12) NOT NULL,
  `unidad` varchar(50) NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `admin_materiaprima`
--

INSERT INTO `admin_materiaprima` (`id`, `nombre`, `proveedor`, `unidad`, `fecha`) VALUES
(8, 'brillo', 21, 'litros', '2020-09-15 22:12:47'),
(9, 'Laca', 21, 'litros', '2020-09-15 22:13:00'),
(7, 'Madera', 22, 'pies', '2020-09-15 22:12:33'),
(10, 'clavo', 20, 'kilos', '2020-09-15 22:13:12'),
(11, 'pijas', 20, 'kilos', '2020-09-15 22:13:34'),
(12, 'triplay rayado', 19, 'hojas', '2020-09-15 22:13:57'),
(13, 'triplay de mdf', 19, 'hojas', '2020-09-15 22:14:22'),
(14, 'Gasolina', 26, 'litros', '2020-09-16 15:49:54');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin_muebles`
--

DROP TABLE IF EXISTS `admin_muebles`;
CREATE TABLE IF NOT EXISTS `admin_muebles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `colores` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `material` text NOT NULL,
  `modelo` varchar(15) NOT NULL,
  `categoria` varchar(50) NOT NULL,
  `foto` varchar(100) NOT NULL,
  `proceso_maquilado` text NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `modelo` (`modelo`),
  UNIQUE KEY `nombre` (`nombre`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `admin_muebles`
--

INSERT INTO `admin_muebles` (`id`, `nombre`, `colores`, `material`, `modelo`, `categoria`, `foto`, `proceso_maquilado`, `fecha`) VALUES
(12, 'Buro con puerta', 'Chocolate, blanco', 'madera de pino', 'bp1', 'buros', 'uploads/5f6221cdb3ee2_buroconpuerta.png', '<h5> Estructura</h5>\n\n4 patas - 63 cm <br>\n5 armadores - 39 cm<br>\n7 puentes - 35 cm<br><br>\n\n<h5>Cajones y puertas</h5>\n\n2 puertas - 35.5 cm<br>\n2 cercos - 25 cm<br>\n4 cajones y peinasos - 32cm<br><br>\n\n<h5>Correderas, batientes y costados</h5>\n\n2 correderas - 37 cm<br>\n2 costados - 35 cm<br>\n1 testero - 32 cm<br>', '2020-09-15 22:17:10');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes_proveedores`
--

DROP TABLE IF EXISTS `clientes_proveedores`;
CREATE TABLE IF NOT EXISTS `clientes_proveedores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_completo` varchar(100) NOT NULL,
  `genero` varchar(10) NOT NULL,
  `telefono` varchar(15) NOT NULL,
  `telefono_emergencia` varchar(15) NOT NULL,
  `correo` varchar(100) NOT NULL DEFAULT 'sin correo',
  `facebook` varchar(100) NOT NULL DEFAULT 'sin facebook',
  `direccion` varchar(100) NOT NULL,
  `tipo` varchar(10) NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `telefono` (`telefono`)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `clientes_proveedores`
--

INSERT INTO `clientes_proveedores` (`id`, `nombre_completo`, `genero`, `telefono`, `telefono_emergencia`, `correo`, `facebook`, `direccion`, `tipo`, `fecha`) VALUES
(17, 'Jaime Francisco Flores Silva', 'Hombre', '7861515454', '8488484888', 'sin correo', 'Jaime Francisco Flores Silva', 'CALLE AGUSTIN LARA NO. 69-B', 'proveedor', '2020-09-15 20:42:19'),
(18, 'Karla Paulette Munguía González', 'Mujer', '7861514444', '4545151555', 'sin correo', 'Karla Munguía', 'AV. INDEPENDENCIA NO. 241', 'proveedor', '2020-09-15 20:43:57'),
(19, 'Jaime Chávez Heredia', 'Hombre', '7861515111', '5454545555', 'sin correo', 'Jaime Chávez', 'CALLE AGUSTIN LARA NO. 69-B', 'proveedor', '2020-09-15 20:45:46'),
(20, 'KARINA GUILLEN MARIN', 'Mujer', '7856595255', '4586454555', 'sin correo', 'kariina guillen', 'CALLE AGUSTIN LARA NO. 69-B', 'proveedor', '2020-09-15 21:01:40'),
(21, 'Miguel Ángel Hernández Prado', 'Hombre', '7894555455', '1221222122', 'miguel@gmail.com', 'Ángel Hernández', 'CALLE MATAMOROS NO. 310', 'proveedor', '2020-09-15 21:03:05'),
(22, 'Yaír Jofrá Moreno Chávez', 'Hombre', '7845494522', '4484785455', 'sin correo', 'sin facebook', 'AV. 5 DE MAYO NO. 1652', 'proveedor', '2020-09-15 21:05:41'),
(26, 'Gasolinera las fuentes', 'Hombre', '0', '0', 'sin correo', 'sin facebook', 'CALLE OCAMPO NO 14', 'proveedor', '2020-09-16 15:49:02'),
(24, 'Luis Felipe Delgado Barrón', 'Hombre', '7845455866', '7844848488', 'sin correo', 'sin facebook', 'BLVD. BENITO JUAREZ S / N', 'cliente', '2020-09-15 21:13:11'),
(25, 'Daniela Ivette Vega Hernández', 'Mujer', '7868484515', '8487485585', 'ivette@gmail.com', 'Ivette Vega', 'CALLE OCAMPO NO 14', 'cliente', '2020-09-15 21:14:47');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `equipo_trabajo`
--

DROP TABLE IF EXISTS `equipo_trabajo`;
CREATE TABLE IF NOT EXISTS `equipo_trabajo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_completo` varchar(100) NOT NULL,
  `genero` varchar(20) NOT NULL,
  `rol` varchar(20) NOT NULL,
  `telefono` varchar(15) NOT NULL,
  `telefono_emergencia` varchar(15) NOT NULL,
  `facebook` varchar(50) NOT NULL,
  `direccion` varchar(100) NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `equipo_trabajo`
--

INSERT INTO `equipo_trabajo` (`id`, `nombre_completo`, `genero`, `rol`, `telefono`, `telefono_emergencia`, `facebook`, `direccion`, `fecha`) VALUES
(3, 'Hansel Andres Espejo Ramos', 'Hombre', 'Maquilador', '4848484888', '4548484884', 'Andres Espejo', 'AV. 20 DE NOVIEMBRE NO 1046', '2020-09-15 16:16:47'),
(4, 'Carlos Alberto Montaño', 'Hombre', 'Armador', '7874548855', '4959565955', 'Carlos', 'BLVD. BENITO JUAREZ ESQ. 20 DE', '2020-09-16 14:23:00'),
(5, 'Yaiza Lobo', 'Mujer', 'Pintor', '4848465552', '5659595959', 'Yaiiza', 'AV. INDEPENDENCIA NO 145', '2020-09-16 14:24:05'),
(6, 'Alonso Baeza', 'Hombre', 'Adornador', '7848488555', '4845484885', 'Alonsin', 'AV. 5 DE MAYO NO. 1226', '2020-09-16 14:25:46');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gastos`
--

DROP TABLE IF EXISTS `gastos`;
CREATE TABLE IF NOT EXISTS `gastos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_materiaprima` varchar(50) NOT NULL,
  `cantidad` int(12) NOT NULL,
  `total` double NOT NULL,
  `debe` double NOT NULL DEFAULT 0,
  `comprovante` varchar(100) NOT NULL DEFAULT 'vacio',
  `fecha` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `gastos`
--

INSERT INTO `gastos` (`id`, `id_materiaprima`, `cantidad`, `total`, `debe`, `comprovante`, `fecha`) VALUES
(13, '14', 5, 450, 0, 'vacio', '2020-09-17 16:06:54'),
(11, '13', 100, 10000, 0, 'vacio', '2020-09-16 05:50:43'),
(12, '12', 50, 12000, 0, 'vacio', '2020-09-16 10:51:11'),
(10, '14', 5, 450, 0, 'vacio', '2020-09-16 15:50:18');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedidos`
--

DROP TABLE IF EXISTS `pedidos`;
CREATE TABLE IF NOT EXISTS `pedidos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `asunto` varchar(100) NOT NULL,
  `descripcion` text NOT NULL,
  `type` varchar(30) NOT NULL,
  `img` varchar(100) NOT NULL,
  `stage_id` int(11) NOT NULL,
  `fecha_registro` timestamp NOT NULL DEFAULT current_timestamp(),
  `fecha_entrega` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `pedidos`
--

INSERT INTO `pedidos` (`id`, `asunto`, `descripcion`, `type`, `img`, `stage_id`, `fecha_registro`, `fecha_entrega`) VALUES
(9, 'pedido uno', '4 buros( 2 chocolate, 2 vino)', 'issue', 'assets/img/fondo.jpg', 2, '2020-09-18 19:51:55', '2020-09-26 05:00:00'),
(10, 'pedido dos', '10 roperos roma color vino', 'issue', 'assets/img/fondo.jpg', 4, '2020-09-18 21:22:09', '2020-09-30 05:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `precios`
--

DROP TABLE IF EXISTS `precios`;
CREATE TABLE IF NOT EXISTS `precios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_mueble` varchar(50) NOT NULL,
  `precio_unitario` double NOT NULL,
  `precio_mayoreo` double NOT NULL,
  `precio_produccion` double NOT NULL,
  `costo_maquilado` double NOT NULL,
  `costo_armado` double NOT NULL,
  `costo_pintado` double NOT NULL,
  `costo_adornado` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `precios`
--

INSERT INTO `precios` (`id`, `id_mueble`, `precio_unitario`, `precio_mayoreo`, `precio_produccion`, `costo_maquilado`, `costo_armado`, `costo_pintado`, `costo_adornado`) VALUES
(4, '12', 550, 450, 350, 50, 50, 50, 50);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `stock_materia_prima`
--

DROP TABLE IF EXISTS `stock_materia_prima`;
CREATE TABLE IF NOT EXISTS `stock_materia_prima` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_materiaprima` int(11) NOT NULL,
  `cantidad` varchar(12) NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `stock_materia_prima`
--

INSERT INTO `stock_materia_prima` (`id`, `id_materiaprima`, `cantidad`, `fecha`) VALUES
(5, 7, '50', '2020-09-17 17:13:55'),
(6, 14, '20', '2020-09-17 19:53:39'),
(7, 13, '50', '2020-09-17 19:54:34');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `stock_muebles`
--

DROP TABLE IF EXISTS `stock_muebles`;
CREATE TABLE IF NOT EXISTS `stock_muebles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_mueble` int(11) NOT NULL,
  `cantidad` int(20) NOT NULL,
  `color` text NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `stock_muebles`
--

INSERT INTO `stock_muebles` (`id`, `id_mueble`, `cantidad`, `color`, `fecha`) VALUES
(5, 12, 30, 'chocolate', '2020-09-17 21:49:56'),
(4, 12, 40, 'vino', '2020-09-17 21:46:16');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ventas`
--

DROP TABLE IF EXISTS `ventas`;
CREATE TABLE IF NOT EXISTS `ventas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_cliente` int(11) NOT NULL,
  `asunto_general` varchar(100) NOT NULL,
  `productos_vendidos` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`productos_vendidos`)),
  `pago` double NOT NULL,
  `total` double NOT NULL,
  `debe` double NOT NULL,
  `fecha_inicio` timestamp NOT NULL DEFAULT current_timestamp(),
  `fecha_fin` timestamp NOT NULL DEFAULT current_timestamp(),
  `status` varchar(10) NOT NULL DEFAULT 'debe',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ventas`
--

INSERT INTO `ventas` (`id`, `id_cliente`, `asunto_general`, `productos_vendidos`, `pago`, `total`, `debe`, `fecha_inicio`, `fecha_fin`, `status`) VALUES
(6, 25, 'njkhnjn', '[{\"id_producto\":\"1\",\"color\":\"\",\"cantidad\":\"1\",\"total\":\"0\"}]', 3300, 3300, 0, '2020-09-15 13:00:00', '2020-09-24 13:00:00', 'pagado'),
(5, 24, 'venta de buros', '[{\"id_producto\":\"1\",\"color\":\"\",\"cantidad\":\"1\",\"total\":\"0\"}]', 3850, 3850, 0, '2020-09-14 23:00:00', '2020-09-17 23:00:00', 'pagado');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
