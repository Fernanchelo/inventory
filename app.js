app.controller('appController', ['$scope', '$http', '$mdDialog','$state','toast','Upload','$timeout', function($scope, $http, $mdDialog,$state,toast,Upload,$timeout) {
    $scope.stock='active';
    $scope.tipo_almacen="";
    $scope.lista = [{
       id_producto:'1',
       color:'',
       cantidad:'1',
       total: '0'
     }];

   $scope.add_lista= function(){
       $scope.lista.push(
           {
           id_producto:'1',
           color:'',
           cantidad:'1',
           precio:'0',
           total: '0'
          });
   };
   $scope.remove_lista= function(index){
       $scope.lista.splice(index, 1);
       $scope.total();
   };

   $scope.cantidad=function(index){
       if($scope.lista[index].precio!=""){
           $scope.lista[index].total=$scope.lista[index].precio * $scope.lista[index].cantidad;
           $scope.total();
       }
   };
   $scope.precios = function(index, lista){
       $scope.lista[lista].precio= $scope.costos[index].precio_unitario;
       $scope.lista[lista].cantidad=1;
       $scope.lista[lista].total=$scope.lista[lista].precio * $scope.lista[lista].cantidad;
       // $scope.dato.total = $scope.dato.total + $scope.lista[index].total
       $scope.total();
   };
   $scope.total= function(){
       var sum=0;
       for (let i = 0; i < $scope.lista.length; i++) {
           sum = sum + $scope.lista[i].total;
       }
       $scope.dato.total= sum;
   };

   $scope.detallemueble= function(uisref, nombre, proceso, foto){
       $scope.nombre=nombre;
       $scope.proceso= proceso;
       $scope.foto= foto;
       $state.go(uisref);
   };

  
     
   $scope.genero=[
       {
           name:'Hombre'
       },
       {
           name:'Mujer'
       }
   ];
   $scope.tipo_gasto=[
       {
          id:1,
          name:'Muebles'
       },{
         id:2,
         name:'Materia Prima'
       }
 
     ];
   $scope.dato={};
   $scope.detalle_datos={};
   $scope.proveedores=[];
   $scope.selection='insert';


 $scope.tooltip=function(){
   $scope.tooltip=tooltip.pop
}
   $scope.series = ['Ventas', 'Gastos'];
         // Paginacion
         $scope.currentPage = 1;
         $scope.itemsPerPage = 5;
       
         $scope.pageChanged = function() {
             console.log($scope.currentPage);
         };

// kanban
// object for stages
$scope.stages =[
               {
                   "id": 1,
                   "name": "Pedidos",
                   "color":"gris" 
               },
               {
                   "id": 2,
                   "name": "En proceso",
                   "color":"naranja" 
               },
               {
                   "id": 3,
                   "name": "Terminados",
                   "color":"verde" 
               },
               {
                   "id": 4,
                   "name": "Aprovados",
                   "color":"azul" 
               },
               {
                   "id": 5,
                   "name": "Rechazados",
                   "color":"rojo" 
               },
               ];

// object for tasks
$scope.tasks = [];

$scope.select_tasks = function() {
   $scope.dato.tabla='pedidos';
   $http.post('services/select_from.php', {
     dato:$scope.dato
 })
 .then(function success(e) {
     $scope.errors = [];
     $scope.tasks = e.data.datos;
 }, function error(e) {
     $scope.errors = e.data.errors;
 });
 return $scope.tasks;

};
$scope.select_tasks();

// function for drag start
$scope.dragStart = function dragStart(event, task){
   task.dragging = true;
};

// function for on dropping
$scope.onDrop = function onDrop(data,event,stage){
   if(data && data.stage_id != stage.id){
       data.showMore = false;
       data.stage_id = stage.id;
       data.dragging = false;
       $scope.dato={};

       $scope.dato.id = data.id;
       $scope.dato.stage_id = stage.id;

       $scope.insert_table('pedidos','update','home.kanban', '');
   };

};	

$scope.newTask = {};

// show more
$scope.showMore = function showMore(task){
   if(task.dragging == true)
       return;

       if(task.showMore == true){
           task.showMore = false;
       }
       else{
           task.showMore = true;	
       }
}


// delete task
$scope.deleteTask = function deleteTask(task){
   var idx = $scope.tasks.indexOf(task);
   $scope.tasks.splice(idx, 1);
}

$scope.exportTasks = function exportTasks(){
   var x = window.open();
   x.document.open();
   x.document.write('<html><body><pre>' + JSON.stringify($scope.tasks, null, 2) + '</pre></body></html>');
   x.document.close();
}

$scope.entradas= function(ide){
   $scope.stock=ide;
  
}


$scope.openModal= function(modal, index, tabla){
   $scope.tabla=tabla;
   $scope.dato=$scope.datos[index];
   var modal_element = angular.element(modal);
   modal_element.modal('show');

};


  

//    conexiones a bd

// subir fotos a servidor
$scope.uploadFiles = function (files) {
   $scope.files = files;         
 };
 
 $scope.subirImg=function(modal){
   
   if ($scope.files && $scope.files.length) {
      Upload.upload({
          url: 'upload.php',
          data: {
              files: $scope.files,
              dato: $scope.dato
          }
      }).then(function (response) {
       $timeout(function () {
           $scope.result = response.data;
       });
      }, function (response) {
          if (response.status=='error') {
           $scope.errorMsg = response.status + ': ' + response.data;
           toast({
             duration: 2000,
             message:  $scope.errorMsg,
             className: "alert-danger"
         });
          }
      }, function (evt) {
       $scope.select_tabla('admin_muebles');
           toast({
             duration: 2000,
             message: "Operación exitosa!",
             className: "alert-success"
         });
         $state.go(modal);
          $scope.files={}; 
          $scope.dato={};
          
      });
  }else{
    var conf = confirm('Sin imagenes seleccionas!');
  }
  
  };
  $scope.updateImg=function(modal){
 
   Upload.upload({
     url: 'update_image.php',
     data: {
         files: $scope.files,
         dato:$scope.dato
     }
 }).then(function (response) {
   
 }, function (response) {
     if (response.status > 0) {
         console.log("Error al modificar");
         $scope.errorMsg = response.status + ': ' + response.data;
     }
 }, function (evt) {

     $scope.select_tabla('admin_muebles');
     $state.go(modal);
     $scope.files={}; 
     $scope.dato={};
 });
  };

// ui select proveedores
$scope.select_proveedores = function() {
   $scope.dato.tabla='clientes_proveedores';
   $http.post('services/proveedores.php', {
     dato:$scope.dato
 })
 .then(function success(e) {
     $scope.errors = [];
     $scope.proveedores = e.data.datos;
 }, function error(e) {
     $scope.errors = e.data.errors;
 });
 return $scope.proveedores;
};

// ui select proveedores
$scope.select_clientes = function() {
   $http.post('services/clientes.php', {
     dato:$scope.dato
 })
 .then(function success(e) {
     $scope.errors = [];
     $scope.clientes = e.data.datos;
 }, function error(e) {
     $scope.errors = e.data.errors;
 });
 return $scope.clientes;
};

// ui select muebles
$scope.select_muebles = function() {
   $scope.dato.tabla='admin_muebles';
   $http.post('services/select_from.php', {
     dato:$scope.dato
 })
 .then(function success(e) {
     $scope.errors = [];
     $scope.muebles = e.data.datos;
 }, function error(e) {
     $scope.errors = e.data.errors;
 });
 return $scope.muebles;
};
// ui select costos
$scope.select_costos = function() {
   
   $http.post('services/costos.php', {
     dato:$scope.dato
 })
 .then(function success(e) {
     $scope.errors = [];
     $scope.costos = e.data.datos;
 }, function error(e) {
     $scope.errors = e.data.errors;
 });
 return $scope.costos;
};
// 
$scope.unidad="";
$scope.costo_unitario=0;
$scope.item=function(id){
   $scope.dato.id_materiaprima=$scope.gastos[id].id;
   $scope.unidad=$scope.gastos[id].unidad;
   $scope.costo_unitario=$scope.gastos[id].costo;
   $scope.dato.total=0;
   $scope.dato.cantidad=0;
   
}
// 

// select gasto
$scope.gastos=[];
$scope.select_gastos = function() {
   $scope.dato.tabla='admin_materiaprima';

   $http.post('services/select_from.php', {
     dato:$scope.dato
 })
 .then(function success(e) {
     $scope.errors = [];
     $scope.gastos = e.data.datos;
 }, function error(e) {
     $scope.errors = e.data.errors;
 });
 return $scope.gastos;
};

 // consulta select
 $scope.select = function(identificador) {
   $http.post(identificador, {
     dato:$scope.dato
 })
 .then(function success(e) {
     $scope.errors = [];
     $scope.datos = e.data.datos;
 }, function error(e) {
     $scope.errors = e.data.errors;
 });
 return $scope.datos;
};

$scope.select_tabla = function(tabla) {
   $scope.dato.tabla=tabla;
   $http.post('services/select_from.php', {
     dato:$scope.dato
 })
 .then(function success(e) {
     $scope.errors = [];
     $scope.datos = e.data.datos;
 }, function error(e) {
     $scope.errors = e.data.errors;
 });
 return $scope.datos;
};

$scope.select_tabla_id   = function(tabla, id) {
   $scope.dato.tabla=tabla;
   $scope.dato.id=id;
   $http.post('services/select_from_id.php', {
     dato:$scope.dato
 })
 .then(function success(e) {
     $scope.errors = [];
     $scope.datos = e.data.datos;
 }, function error(e) {
     $scope.errors = e.data.errors;
 });
 return $scope.datos;
};

// consulta eliminar
$scope.delete = function(index, identificador, tabla) {
   $scope.eliminar={};
   if(tabla=='pedidos'){
       $scope.eliminar=$scope.tasks[index];
   }else{
       $scope.eliminar=$scope.datos[index];
   }
   
   $scope.eliminar.tabla=tabla;
   var conf = confirm("¿Realmente quieres eliminar?");
   if (conf == true) {
       $http.post(identificador, {
               dato: $scope.eliminar
           })
           .then(function success(e) {
               if (e.data.error != "") {
                   toast({
                       duration: 2000,
                       message: e.data.error,
                       className: "alert-danger"
                   });  
               } else {
                   $scope.errors = [];
                   if(tabla=='pedidos'){
                       $scope.tasks.splice(index, 1);
                   }else{
                  
                   $scope.datos.splice(index, 1);
               }
                   toast({
                       duration: 2000,
                       message: "Datos Eliminados!",
                       className: "alert-success"
                   });
               }
           }, function error(e) {
               $scope.errors = e.data.errors;
           });
   }
 };
$scope.editar = function(modal, index){
   $scope.selection='update';
   $scope.dato = $scope.datos[index];

   $state.go(modal);
}

$scope.insertar = function(modal){
   $scope.selection='insert';
   $scope.dato={};
   $state.go(modal);
}

 $scope.insert_table = function(tabla, accion, uisref, datos) {
   $scope.dato.tabla=tabla;
   $scope.dato.accion=accion;
   if(tabla=='ventas'){
       $scope.dato.productos_vendidos='';
       $scope.dato.productos_vendidos = JSON.stringify($scope.lista);
       if( $scope.dato.debe > 0){
           $scope.dato.status='debe';
       }else{
           $scope.dato.status='pagado';
       }
   }

   if(tabla=='pedidos'){
       $scope.dato.img='assets/img/fondo.jpg'; 
       $scope.dato.type='issue';
   }

   if(tabla=='stock_materia_prima' && accion=='update'){

       $scope.id=$scope.dato.id;
       $scope.id_materiaprima=$scope.dato.id_materiaprima;

       if($scope.add_tipo=="entradas"){
           $scope.cantidad=parseInt($scope.dato.cantidad)+parseInt($scope.dato.cant);
       }else if($scope.add_tipo=="salidas"){
           $scope.cantidad=parseInt($scope.dato.cantidad)-parseInt($scope.dato.cant);
       }else{
           $scope.cantidad=$scope.dato.cantidad;
       }
       
       $scope.dato={};

       $scope.dato.id_materiaprima=$scope.id_materiaprima;
       $scope.dato.cantidad=$scope.cantidad;
       $scope.dato.accion=accion;
       $scope.dato.tabla=tabla;
       $scope.dato.id=$scope.id;
   }

   if(tabla=='stock_muebles' && accion=='update'){

       $scope.id=$scope.dato.id;
       $scope.id_mueble=$scope.dato.id_mueble;

       if($scope.add_tipo=="entradas"){
           $scope.cantidad=parseInt($scope.dato.cantidad)+parseInt($scope.dato.cant);
       }else if($scope.add_tipo=="salidas"){
           $scope.cantidad=parseInt($scope.dato.cantidad)-parseInt($scope.dato.cant);
       }else{
           $scope.cantidad=$scope.dato.cantidad;
       }
       
       $scope.dato={};

       $scope.dato.id_mueble=$scope.id_mueble;
       $scope.dato.cantidad=$scope.cantidad;
       $scope.dato.accion=accion;
       $scope.dato.tabla=tabla;
       $scope.dato.id=$scope.id;
   }

   $http.post('crud/insert_table.php', {
           dato: $scope.dato
       })
       .then(function success(e) {
           if (e.data.error != "") {
               toast({
                   duration: 2000,
                   message: e.data.error,
                   className: "alert-danger"
               });
           } else {
               $scope.errors = [];
              if(!datos){
                  if(tabla=='pedidos'){
                   $scope.select_tasks();
                  }else{
                   $scope.select_tabla(tabla);
                  }
               
              }else{
               $scope.select(datos);
              }

              $scope.dato = {};
              if(uisref==null){
          
              }else{

                   if(uisref=='.bd-entradas-modal-sm'){
                       var modal_element = angular.element('.bd-entradas-modal-sm');
                       modal_element.modal('hide');
                   }else{
                  $state.go(uisref);
                   }
              }
                        
               toast({
                   duration: 2000,
                   message: "Operación exitosa!",
                   className: "alert-success"
               });

           }
       }, function error(e) {
           $scope.errors = e.data.errors;
       });
  
   
  };

  

  $scope.add_pago = function(cant){
   
   var aux=$scope.dato.debe-cant;
   if(aux >= 0){
       if($scope.tabla=='ventas'){
           $scope.dato.pago = parseInt($scope.dato.pago) + parseInt(cant);
           $scope.dato.debe = parseInt($scope.dato.debe) - parseInt(cant);

           $scope.insert_table(tabla,'update','home.ventas', 'services/ventas.php');
           var modal_element = angular.element('#addpago');
           modal_element.modal('hide');
           $scope.pago=0;          
       } 
       if($scope.tabla=='gastos'){
           $scope.dato.debe = parseInt($scope.dato.debe) - parseInt(cant);
           $scope.insert_table('gastos','update','home.gastos', 'services/gastos.php');
           var modal_element = angular.element('#addpago');
           modal_element.modal('hide');
           $scope.pago=0; 

       }
   }else{
       toast({
           duration: 2000,
           message: 'No se pude realizar un pago mayor al lo que se debe',
           className: "alert-danger"
       });

       var modal_element = angular.element('#addpago');
       modal_element.modal('hide');
       $scope.pago=0;
   }
};

$scope.add_entradas = function(index, tipo, titulo){
   $scope.dato=$scope.datos[index];
   $scope.tipo_almacen=titulo;
   if(titulo=='materiaprima'){
       $scope.titulo="Materia prima";
   }else{
       $scope.titulo="Muebles";
   }
   $scope.add_tipo=tipo;
   var modal_element = angular.element('.bd-entradas-modal-sm');
   modal_element.modal('show');
};

 $scope.etiquetas = [];
$scope.series = ['muebles'];

$scope.datos_chart = [];

$scope.estadisticas= function(){
    $http.post('services/grafica.php', {
        dato:$scope.dato
    })
    .then(function success(e) {
        $scope.errors = [];
        $scope.datos_char = e.data.datos;
    }, function error(e) {
        $scope.errors = e.data.errors;
    }); 

    $scope.datos_Grafica();
};

$scope.datos_Grafica= function(){
    $scope.etiquetas = [];
    $scope.datos_chart = [];
    
    $timeout(function() {
        for (let i = 0; i < $scope.datos_char.length; i++) {
            $scope.etiquetas.push($scope.datos_char[i].mes);
            $scope.datos_chart.push( parseFloat($scope.datos_char[i].valores));
       }
      },1000);
  
};




}]);

app.directive('tooltip', function(){
 return {
     restrict: 'A',
     link: function(scope, element, attrs){
         element.hover(function(){
             // on mouseenter
             element.tooltip('show');
         }, function(){
             // on mouseleave
             element.tooltip('hide');
         });
     }
 };
});

app.directive('numberConverter', function() {
   return {
     priority: 1,
     restrict: 'A',
     require: 'ngModel',
     link: function(scope, element, attr, ngModel) {
       function toModel(value) {
         return "" + value; // convert to string
       }
 
       function toView(value) {
         return parseInt(value); // convert to number
       }
 
       ngModel.$formatters.push(toView);
       ngModel.$parsers.push(toModel);
     }
   };
 });