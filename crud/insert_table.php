<?php
function convertDateTime($date, $format = 'Y-m-d H:i:s')
{
    $tz1 = 'UTC';
    $tz2 = 'Mexico/General'; // UTC +7

    $d = new DateTime($date, new DateTimeZone($tz1));
    $d->setTimeZone(new DateTimeZone($tz2));

    return $d->format($format);
}

$data = json_decode(file_get_contents('php://input'), TRUE);
$validation_error = '';
if (isset($data['dato'])) {
    include ('../assets/class/library.php'); 

    $accion = (isset($data['dato']['accion']) ? $data['dato']['accion'] : NULL);
    $tabla = (isset($data['dato']['tabla']) ? $data['dato']['tabla'] : NULL);

    $c = ""; 
    $v = "";
    $m = "";

    foreach ($data['dato'] as $clave => $valor) {
        if($clave=='id' || $clave=='tabla' || $clave=='accion'){

        }

        else{

          if($clave=='fecha_inicio' || $clave=='fecha_fin' || $clave=='fecha' || $clave=='fecha_entrega'){
            $valor=convertDateTime($valor);
          }

          if($tabla=='user' && $accion=='update' && $clave=='cuenta' || $clave=='pass' || $clave=='nombre_proveedor'){

          }else{

            if ($tabla=='stock_muebles' && $clave=='nombre' || $tabla=='precios' && $clave=='nombre'  || $tabla=='admin_materiaprima' && $clave=='nombre_proveedor' || $tabla=='gastos' && $clave=='nombre' || $tabla=='ventas' && $clave=='nombre' ){

            }else{
            

                $c=$c.$clave.", ";

                $v=$v."'".$valor."'".", ";
                
                $m=$m.$clave."='".$valor."', ";
              
            
            }
          }
           
        }       
    }
    
      $c=substr($c, 0, -2);
      $v=substr($v, 0, -2);
      $m=substr($m, 0, -2);
      $crud = new Crud();
      if($accion=='insert'){
                  
        $sql="INSERT INTO $tabla(id, $c) VALUES (null, $v)";
        //  echo $sql; 
        $validation_error = $crud->insert($sql);
          
      }elseif($accion=='update'){


        $id = (isset($data['dato']['id']) ? $data['dato']['id'] : NULL);
        $sql="UPDATE $tabla SET $m WHERE id='".$id."'";
        //echo $sql; 
        $validation_error = $crud->insert($sql);
       
      }
    $output = array('error' => $validation_error);     
}  
    echo json_encode($output);
?>