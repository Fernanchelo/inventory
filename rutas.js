var app = angular.module('app', ['ngSanitize', 'ui.router','ngAnimate','ngMaterial', 'ngMessages', 'material.svgAssetsCache','ui.bootstrap','ui.select', 'moment-picker','chart.js','ngDraggable','angularjsToast','ngFileUpload']);
app.config(function($stateProvider, $urlRouterProvider) {
    var states = [{
        name: 'home',
        url: '/home',
        templateUrl: 'views/home.html',
    //     resolve: {
    //       user: function(User) {
    //           console.log('Verificando auntentificacion');
    //           return User.checkAuthentication();
    //       }
    //   }  
    },       
    {
        name: 'home.estadisticas',
        url: '/Estadisticas',
        templateUrl: 'views/estadisticas.html'
    },       
    {
        name: 'home.almacen',
        url: '/Almacen',
        templateUrl: 'views/almacen.html'
    },       
    {
        name: 'home.almacenMuebles',
        url: '/Almacen Muebles',
        templateUrl: 'views/almacen_muebles.html'
    },       
    {
        name: 'home.almacenMuebles.add',
        url: '/add',
        templateUrl: 'views/modal-almacen-muebles.html'
    },       
    {
        name: 'home.almacenMateriaPrima',
        url: '/Almacen Materia prima',
        templateUrl: 'views/almacen_materiaprima.html'
    },
    {
        name:'home.almacenMateriaPrima.add',
        url: '/add',
        templateUrl: 'views/modal-almacenmateriaprima.html',
    },        
    {
        name: 'home.clientes',
        url: '/Clientes',
        templateUrl: 'views/clientes.html'
    }, 
    {
        name:'home.clientes.add',
        url: '/add',
        templateUrl: 'views/modal-clientes.html',
    },      
    {
        name: 'home.proveedores',
        url: '/Proveedores',
        templateUrl: 'views/proveedores.html'
    }, 
    {
        name:'home.proveedores.add',
        url: '/add',
        templateUrl: 'views/modal-proveedores.html',
    },      
    {
        name: 'home.equipo_trabajo',
        url: '/Equipo_de_trabajo',
        templateUrl: 'views/equipo_trabajo.html'
    },
     {
        name:'home.equipo_trabajo.add',
        url: '/add',
        templateUrl: 'views/modal-equipo_trabajo.html',
    } ,      
            
    {
        name: 'home.costos_produccion',
        url: '/Costos_produccion',
        templateUrl: 'views/costos_produccion.html'
    },
    {
       name:'home.costos_produccion.add',
       url: '/add',
       templateUrl: 'views/modal-costos_produccion.html',
   },       
    {
        name: 'home.gastos',
        url: '/Gastos',
        templateUrl: 'views/gastos.html'
    },
    {
       name:'home.gastos.add',
       url: '/add',
       templateUrl: 'views/modal-gastos.html',
   },       
    {
        name: 'home.ventas',
        url: '/Ventas',
        templateUrl: 'views/ventas.html'
    },
    {
       name:'home.ventas.add',
       url: '/add',
       templateUrl: 'views/modal-ventas.html',
   },{
    name: 'home.kanban',
    url: '/Tareas',
    templateUrl: 'views/kanban.html'
},{
    name: 'home.muebles',
    url: '/Muebles',
    templateUrl: 'views/muebles.html'
},{
    name: 'home.muebles.add',
    url: '/add',
    templateUrl: 'views/modal-muebles.html'
} ,{
    name: 'home.vermueble',
    url: '/Detalles_Mueble',
    templateUrl: 'views/vermueble.html'
},{
    name:'home.materiprima',
    url: '/Materia prima',
    templateUrl: 'views/materiaprima.html',
},  {
    name:'home.materiprima.add',
    url: '/add',
    templateUrl: 'views/modal-materiaprima.html',
}   

  ]
  $urlRouterProvider.otherwise('home');
  states.forEach(function(state) {
    $stateProvider.state(state);
  });
  });
  app.run(function($state) {
     $state.go('home.estadisticas');
//   window.myAppErrorLog = [];
//   $state.defaultErrorHandler(function(error) {
//       // This is a naive example of how to silence the default error handler.
//       window.myAppErrorLog.push(error);
//       console.log('Error al autenticar, redirigiendo a login')
//       $state.go('login')
//   });

})